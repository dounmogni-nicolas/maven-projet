##stage1 build projet maven 
FROM  maven:3.8.6-openjdk-8 as builder 

RUN mkdir /app

WORKDIR /app

COPY . . 

RUN mvn clean install package

##stage2 RUN the image with tomcat

FROM tomcat:8-jre8
COPY --from=builder /app/target/*.war /usr/local/tomcat/webapps

